handler() {
	echo "Hello, from $(bash --version | head -n1)"

	echo $(cat /proc/version)
	echo $(uname -a)
	echo $(uname -r)

	echo $(gcc -v)

	echo $PWD
	echo $(where curl)
}

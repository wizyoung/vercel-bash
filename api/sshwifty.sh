build() {
	echo "Downloading sshwifty"
    curl -L https://github.com/nirui/sshwifty/releases/download/0.2.12-beta-release-prebuild/sshwifty_0.2.12-beta-release_linux_amd64.tar.gz -O
    mkdir sshwifty
    tar -xzvf sshwifty_0.2.12-beta-release_linux_amd64.tar.gz -C ./sshwifty
    chmod +x ./sshwifty/sshwifty_linux_amd64
    echo "building done"
}

handler() {
    SSHWIFTY_LISTENPORT=8081 ./sshwifty/sshwifty_linux_amd64
}
